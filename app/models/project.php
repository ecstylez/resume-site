<?php

namespace Model;

/**
 * Project class
 */
class Project extends Base {

    private $projects;

    public function __construct()
    {
        // construct model method
        parent::__construct();

        $this->setProjects();
    }

    public function getProjects(): Array
    {
        return $this->projects;
    }

    private function setProjects()
    {
        $this->projects = [
            [
                'name'        => 'Quick Shell Scripts',
                'description' => '<strong>Problem:</strong>
                    Have you ever worked with a ticketing system (Pivotal Tracker, JIRA) where you can tie your git commits to tickets by a ticket identifier?

                    <strong>Solution:</strong>
                    Wrote a couple of handy shell scripts to build a commit string that works with project management tools.
                    ' . $this->creditedTo('Working Buildings'),
            ],

            [
                'name'        => 'Company Intranet',
                'description' => '<strong>Problem:</strong>
                    A company is finding it increasingly difficult to locate past paperwork, find storage for new paper work, and a way to manage all of this quickly and efficiently.

                    <strong>Solution:</strong>
                    Created an intranet that allowed to the company to go fully paperless, among other future enhancements.
                    ' . $this->creditedTo('Suncoast Marketing'),
            ],

            [
                'name'        => 'Code Compliance',
                'description' => '<strong>Problem:</strong>
                    The code base has been updated by numerous developers over the years, but hasn\'t adhered to any coding standards.

                    <strong>Solution:</strong>
                    Wrote build and lint scripts using node packages, to assist developers to adhere to coding standards as they\'re writing code.
                    ' . $this->creditedTo('Working Buildings'),
            ],
        ];
    }

    private function creditedTo($string = ''): string
    {
        if (empty($string))
        {
            return "";
        }

        return "<span class=\"credited-to\"><strong>company : </strong> {$string}</span>";
    }
}