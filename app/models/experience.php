<?php

namespace Model;

class Experience extends Base {

    private $experiences;

    public function __construct()
    {
        // construct model method
        parent::__construct();

        $this->setExperiences();
    }

    public function getExperiences(): Array
    {
        return $this->experiences;
    }

    private function setExperiences()
    {
        $this->experiences = [
            [
                'duration'    => '2019 - Present',
                'company'     => 'Working Buildings',
                'city'        => 'Atlanta, GA',
                'position'    => 'PHP Developer',
                'description' => 'Joined the CxAlloy development team under Working Buildings to assist with accelerating the development of the company’s Facility Management suite.

                    Some tasks included : writing lint and build scripts from the ground up using Node, writing linux command shell scripts to expedite repetitive tasks, writing up documentation and scalable scripts for use within the site’s existing architecture, such as :
                    - an in-house selectpicker script (JS, SCSS)
                    - an administration dashboard to quickly navigate client information (PHP, JS, SCSS)
                    - built a notification system (PHP, JS)

                    Other tasks included joining the company’s Windows app development team to assist in expediting the development timeline (using BackboneJS MV* coupled with NodeJS running on Electron)',
            ]

        ];
    }


}