<?php

namespace Model;

class Proficiency extends Base {

    private $proficiencies;

    public function __construct()
    {
        // construct model method
        parent::__construct();

        $this->setProficiencies();
    }

    public function getProficiencies(): Array
    {
        return $this->proficiencies;
    }

    private function setProficiencies()
    {
        $this->proficiencies = [
            'php'     => 'PHP (7.*)',
            'js'      => 'Javascript (ES6)',
            'node'    => 'Node JS',
            'sass'    => 'Sass',
            'css3'    => 'CSS3',
            'symfony' => 'Symfony 4.*',
            'aws'     => 'Amazon Web Services',
        ];
    }
}