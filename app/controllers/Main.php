<?php

namespace Resume\Controller;

use Model;

class Main {

    public function __construct()
    {

    }

    public function getDetails()
    {
        $details = [
            'proficiencies' => (new Model\Proficiency())->getProficiencies(),
            'experiences'   => (new Model\Experience())->getExperiences(),
            'projects'      => (new Model\Project())->getProjects(),
        ];

        return $details;
    }
}