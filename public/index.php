<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	// autoloaders
	$loader   = require __DIR__ . '/../vendor/autoload.php';

	$main = new Resume\Controller\Main();
	$data = $main->getDetails();

	// statics (for now)
	$subject     = urlencode("Hi Mark, I just viewed your online business card");
	$linkedin    = "https://www.linkedin.com/in/mark-eccleston-996a2b5/";
	$email       = 'mailto:m.ecstylez@gmail.com?subject=' . $subject;
	$resume_link = "https://docs.google.com/document/d/e/2PACX-1vR3Ij6OcKV5rVkjZJu9PFkwmo8ik-ej_Ya_PMFecYXKUa_5LNjA221a9Eqd1dy_Ztan0Nxqa-PF0p-w/pub";

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Mark Eccleston || Software Engineer | Atlanta, GA</title>
	<!-- bootstrap grid -->
	<link rel="stylesheet" href="assets/vendors/bootstrap/css/bootstrap.min.css">

	<!-- google fonts -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;500&display=swap" rel="stylesheet">

	<!-- fontawesome -->
	<link rel="stylesheet" href="assets/vendors/fontawesome/css/all.min.css">

	<!-- aos -->
	<link rel="stylesheet" href="assets/vendors/aos/aos.css">

	<!-- stylesheet -->
	<link rel="stylesheet" href="assets/css/resume.css">
</head>
<body>
<nav class="navbar fixed-top">
	<div class="container">
		<div class="navbar-header">
			<span class="brand-icon fa-stack">
				<i class="fa fa-circle fa-stack-2x"></i>
				<span class="fa-stack-1x">M<strong>E</strong></span>
			</span>
			<a class="navbar-brand" href="#">Mark <strong>Eccleston</strong></a>
		</div>
		<div id="navbar">
			<ul class="nav">
				<li class="nav-item">
					<a class="nav-link active" href="#hi_its_me">About Me</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= $resume_link; ?>" target="_blank">Resume</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="mailto:<?= $email; ?>" title="Contact Me!">Contact</a>
				</li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>
<div class="container">

	<div id="hi_its_me" class="row justify-content-center page-section">
		<div class="col-md-3">
			<div class="justify-content-center">
				<div class="photo-border">
					<div class="photo-inner"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<article id="about_me">
				<p id="job_title">Software Engineer</p>
				<h1 id="my_name">Mark Eccleston</h1>

				<article id="my_story">
					<p>
						Hi! My name's Mark, and I write code! I was born in Miami, FL, grew up in Fort Lauderdale - and recently, relocated to Atlanta, GA. This city's awesome! Great food, history, culture... and a growing tech hub!
					</p>
					<p>
						When it comes to work experience, I've been developing for 10+ years, both frontend and backend. Specializing mainly in PHP, and quickly building up some experience with ReactJS.
					</p>
					<p>
						Like most developers, I've got a handful of proficiencies under the belt, and a personality to boot! Take a look around and see if I'm what you're looking for!
					</p>
				</article>
				<p>

					<?php
						$links = [
							'fas fa-at'        =>
								[
									'tooltip' => 'Email Me!',
									'href'    => $email,
								],
							'fab fa-linkedin'  =>
								[
									'tooltip' => 'Visit my LinkedIn Page',
									'href'    => $linkedin,
								],
							'fab fa-bitbucket' =>
								[
									'tooltip' => 'Browse my Repo',
									'href'    => 'https://bitbucket.org/ecstylez/',
								],
						];

						foreach ($links as $class => $details)
						{
					?>
						<a href="<?= $details['href']; ?>">
							<span class="fa-stack" title="<?= $details['tooltip']; ?>">
								<i class="fas fa-circle fa-stack-2x"></i>
								<i class="<?= $class; ?> fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					<?php
						}
					?>
				</p>
			</article>
		</div>
	</div>

	<hr>

	<div id="work_experience" class="row page-section">
		<div class="col-sm">
			<div class="container">
				<div class="row">
					<div class="col-sm">
						<h3 data-aos="flip-down">
							Work Experience <strong>Spotlight</strong>
						</h3>
					</div>
				</div>

				<?php foreach ($data['experiences'] as $experience) { ?>

					<div class="row justify-content-center" data-aos="fade-up">
						<div class="col-sm-3 col-md-2">
							<div class="job-details">
								<strong><?= $experience['duration']; ?></strong>
								<span class="company"><?= $experience['company']; ?></span>
								<span class="location"><?= $experience['city']; ?></span>
							</div>
						</div>
						<div class="col-sm-9 col-md-9 description-border">
							<div class="job_title"><?= $experience['position']; ?></div>
							<div class="job_description">
								<?= nl2br($experience['description']); ?>
							</div>
						</div>
					</div>

				<?php } ?>
				<div class="row" data-aos="fade-up" data-aos-delay="100">
					<div class="col-sm text-center">
						<a href="<?= $linkedin; ?>" target="_blank" class="btn download-resume-btn justify-content-center">
							<i class="far fa-file-alt"> </i> Full Work History
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<div id="proficiencies" class="row page-section">
		<div class="col-sm">
			<h3 data-aos="flip-down">
				<strong>Proficiencies</strong>
			</h3>

			<?php
				$stack_index = 0;
			?>
			<div class="icons" data-aos="fade-right">
				<?php foreach ($data['proficiencies'] as $tech => $tooltip) { ?>
					<span class="icon" title="<?= $tooltip; ?>"
						data-aos="fade-right"
						data-aos-delay="<?= 150 * $stack_index++; ?>"
						>
						<span class="fa-stack fa-2x">
							<i class="fas fa-square fa-stack-2x"></i>
							<i class="fab fa-<?= $tech; ?> fa-stack-1x fa-inverse"></i>
						</span>
					</span>
				<?php } ?>
			</div>
		</div>
	</div>

	<hr>

	<div id="project_examples" class="row page-section">
		<div class="col-sm">
			<div class="container">
				<div class="row">
					<div class="col-sm">
						<h3 data-aos="flip-down">
							Examples of <strong>Past Projects</strong>
						</h3>
					</div>
				</div>

				<div class="row justify-content-center" data-aos="fade-up">
					<div class="col-sm">
						<div id="carousel-container" class="carousel slide" data-ride="carousel" data-interval="<?= 1000 * 15 ?>">
							<div class="carousel-inner">

								<?php
									foreach ($data['projects'] as $key => $project)
									{
										$active = $key === 0 ? 'active' : '';
								?>
									<div class="carousel-item <?= $active; ?>">
										<div class="carousel-caption d-md-block">
											<h5 class="project-title"><?= $project['name']; ?></h5>
											<p><?= nl2br($project['description']); ?></p>
										</div>
									</div>

								<?php } ?>
							</div>
							<a class="carousel-control-prev" href="#carousel-container" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carousel-container" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<div class="row page-section">
		<div class="col-sm">
			<div class="container" id="the_point">
				<div class="row">
					<div class="col-sm">
						<h3 data-aos="flip-down">
							Let's Cut to <strong>the Chase</strong>
						</h3>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-sm-10" data-aos="fade-up">
						<article>
							<p>
								Rather than bore you with all the fluff, about my work experience, my schooling, or even a long drawn out sentence to try to sell you on my work ethic... I'd rather cut straight to the point.
							</p>
							<p>
								If you have any inkling that I may be what you're looking for, let's connect!
							</p>
						</article>
					</div>

					<div class="w-100"></div>


					<a href="mailto:<?= $email; ?>" class="btn contact-me-btn" data-aos="fade-up">
						<i class="far fa-envelope-open"></i> Contact Me
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<footer>
	Mark Eccleston <span class="spacer">|</span> Software Engineer <span class="spacer">|</span> <?= date('F d, Y'); ?>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/vendors/aos/aos.js"></script>
<script type="text/javascript">
	AOS.init();
</script>
</body>
</html>