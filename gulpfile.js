"use strict";

// packages
const gulp = require('gulp'),
	concat = require('gulp-concat'),
	sass   = require('gulp-sass'),
	rename = require('gulp-rename'),
	babel  = require('gulp-babel'),
	log    = require('fancy-log');

const config = {
	compile : {
		scss : 'compile/scss/'
	}
};

gulp.task('watchscss', function() {
	let files = config.compile.scss + "*.scss";

	gulp.watch(files, {usePolling: true}, gulp.parallel('buildscss'));
});

gulp.task('buildscss', function() {
	let files = config.compile.scss + "*.scss";

	return gulp
		.src(files)
		.pipe(sass({outputStyle: 'compressed'})).on('error', sass.logError)
		.pipe(rename('resume.css'))
		.pipe(gulp.dest('./public/assets/css/'));

});

const build = gulp.series('buildscss');

exports.default = build;
exports.watch   = gulp.parallel('watchscss');